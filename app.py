from flask import Flask
from flask import render_template

from flask import jsonify
from flask import request

from retrieve_results import retrieve_results
from retrieve_suggestions import retrieve_suggestions

app = Flask(__name__)



@app.route('/')
def home():
    return render_template('index.html')



@app.route('/get_suggestions', methods=['POST'])
def get_suggestions():
    keyword = request.form['keyword']
    suggestions = retrieve_suggestions(keyword)
    return jsonify(suggestions)


@app.route('/search', methods=['POST'])
def search():
    keyword = request.form['keyword']
    suggestions = retrieve_suggestions(keyword)
    results = retrieve_results(keyword)
    return render_template('results.html', keyword=keyword, suggestions=suggestions, results=results)














# def retrieve_suggestions(keyword):
#     suggestions = []
#     with open('queries.txt', 'r') as file:
#         lines = file.readlines()
#         for line in lines:
#             words = line.strip().split()
#             for word in words:
#                 if len(word) >= 3 and word.startswith(keyword):
#                     suggestions.append(word)

#     return suggestions

# def retrieve_results(keyword):
#     results = []
#     with open('file.txt', 'r') as file:
#         lines = file.readlines()
#         for line in lines:
#             if keyword in line:
#                 results.append(line)

#     return results



























#/////////////////////////////////////////////////
# def search_in_file(query):
#     file_path = 'file.txt'
#     with open(file_path, 'r') as file:
#         lines = file.readlines()
#     results = [line.strip() for line in lines if query in line]
#     return results, ''.join(lines)

#/////////////////////////////////////////////////
# @app.route('/serch')
# def serch():
#     return render_template('index1.html')

#/////////////////////////////////////////////////
# @app.route('/getCorrection', methods=['POST'])
# def getCorrection():
#     data = request.get_json()
#     words = model.check_word(data['words'])
    
#     return jsonify(words)








#/////////////////////////////////////////////////
# with open('queries.txt', 'r') as file:
#     words = file.read().splitlines()

# @app.route('/suggestions', methods=['GET'])
# def suggestions():
#     query = request.args.get('query')  # الكلمة المدخلة للبحث
#     suggestions = [word for word in words if word.startswith(query)]  # الاقتراحات المبدئية
#     return jsonify({'suggestions': suggestions})


# #/////////////////////////////////////////////////
# @app.route('/')
# def index():
#     return render_template('search.html')

# #/////////////////////////////////////////////////
# @app.route('/search', methods=['GET'])
# def search():
#     query = request.args.get('query')
#     results = search_in_file(query)  
#     # return jsonify({'query': query, 'results': results})
#     return render_template('search.html', query=query, results=results)

# #/////////////////////////////////////////////////
# def search_in_file(query):
#     file = open('file.txt', 'r')
#     lines = file.readlines()
#     file.close()
#     results = [line.strip() for line in lines if query in line]
#     return results



if __name__ == '__main__':
    app.run()