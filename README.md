



# Define the function to read files and create the autocomplete index
def read_files(folder_path):
    # Code to read files and extract queries

def suggest_alternative_queries(query, folder_path):
    # Code to suggest alternative queries based on search logs

def Complete(query, folder_path):
    # Code to create autocomplete index and return search results

# Define the function to calculate the Levenshtein distance between two strings
def levenshtein_distance(s1, s2):
    # Code to calculate Levenshtein distance

# Define the function to suggest corrections for the user's query
def suggest_corrections(query, queries):
    # Code to suggest corrections based on Levenshtein distance

# Define the function to handle the search
def search():
    # Code to handle search functionality

# Create the Tkinter window
window = tk.Tk()
window.title("Search Interface")

# Create the search label and entry
search_label = tk.Label(window, text="Enter your search query:")
search_label.pack()
search_entry = tk.Entry(window)
search_entry.pack()

# Create the search button and the results label
search_button = tk.Button(window, text="Search", command=search)
search_button.pack()
results_label = tk.Label(window, text="Search results:")
results_label.pack()

# Create the text widget for the search results
result_text = tk.Text(window)
result_text.pack()

# Initialize the autocomplete index and the folder path
folder_path = "C:/Users/ASUS/Desktop/New folder"
queries = read_files(folder_path)
results = []

# Start the Tkinter event loop
window.mainloop()





#ُEvaluation



# Load Ground Truth
def load_ground_truth(file_path):
    ground_truth = {}
    with open(file_path, 'r') as f:
        for line in f:
            query_id, corpus_id, score = line.strip().split('\t')
            if query_id.isdigit():
                query_id = int(query_id)
                if int(score) > 0:  # only add relevant documents
                    if query_id not in ground_truth:
                        ground_truth[query_id] = set()
                    ground_truth[query_id].add(corpus_id)
    return ground_truth

file_path = "D:\5 year\.ir_datasets\beir\climate-fever\qrels\test.tsv"
ground_truth = load_ground_truth(file_path)

# Load Matching Truth
file_path = "matching.tsv"
def load_matching_truth():
    matching_truth = {}
    with open(file_path, 'r') as f:
        for line in f:
            values = line.strip().split('\t')
            if len(values) >= 3:
                query_id, doc_title, score = values
                if query_id.startswith("query"):
                    query_id = int(query_id[5:]) 
                    if query_id not in matching_truth:
                        matching_truth[query_id] = set()
                    doc_title = re.sub(r'\d+|\([^)]*\)', '', doc_title)
                    doc_title = re.sub(r'[^\w\s]', '', doc_title)
                    doc_title = doc_title.lstrip('_')  # remove leading underscores
                    matching_truth[query_id].add(doc_title)
    return matching_truth

matching_truth = load_matching_truth()

# Calculate Precision@10
def calculate_precision_at_k(query_id, matching_truth, ground_truth, k=10):
    if query_id in matching_truth:
        matching_docs = list(matching_truth[query_id])[:k]
        relevant_docs = set(ground_truth[query_id])
        retrieved_docs = set(matching_docs)
        not_retrieved_docs = relevant_docs - retrieved_docs
        num_relevant = len(relevant_docs)
        num_retrieved = len(retrieved_docs)
        num_not_retrieved = len(not_retrieved_docs)
        precision = len(relevant_docs.intersection(retrieved_docs)) / num_retrieved if num_retrieved > 0 else 0.0
        return retrieved_docs, not_retrieved_docs, precision
    else:
        return None, None, None

precision_dict = {}
for query_id in matching_truth:
    retrieved_docs, not_retrieved_docs, precision = calculate_precision_at_k(query_id, matching_truth, ground_truth, k=10)
    if retrieved_docs is not None:
        precision_dict[query_id] = precision

# Write Precision@10 values to a file
file_path = 'precision_at_10.txt'
with open(file_path, 'w') as f:
    for query_id, precision in precision_dict.items():
        if precision > 0:
            f.write(f"Query {query_id}: {precision}\n")

# Calculate Recall@10
def calculate_recall_at_k(query_id, matching_truth, ground_truth, k=10):




    if query_id in matching_truth:
        matching_docs = list(matching_truth[query_id])[:k]
        relevant_docs = set(ground_truth[query_id])
        retrieved_docs = set.


import numpy as np

# precision@10-after
####################
# Compute for each query
def calculate_precision_at_k(query_id, matching_truth, ground_truth, k=10):
    if query_id in matching_truth:
        matching_docs = list(matching_truth[query_id])[:k]
        relevant_docs = set(ground_truth[query_id])
        retrieved_docs = set(matching_docs)
        not_retrieved_docs = relevant_docs - retrieved_docs
        num_relevant = len(relevant_docs)
        num_retrieved = len(retrieved_docs)
        num_not_retrieved = len(not_retrieved_docs)
        precision = len(relevant_docs.intersection(retrieved_docs)) / num_retrieved if num_retrieved > 0 else 0.0
        return retrieved_docs, not_retrieved_docs, precision
    else:
        return None, None, None

precision_dict = {}
for query_id in matching_truth:
    retrieved_docs, not_retrieved_docs, precision = calculate_precision_at_k(query_id, matching_truth, ground_truth, k=10)
    if retrieved_docs is not None:
        precision_dict[query_id] = precision

# Write Precision@10 values to a file
file_path = 'precision_at_10after.txt'
with open(file_path, 'w') as f:
    for query_id, precision in precision_dict.items():
        if precision > 0:
            f.write(f"Query {query_id}: {precision}\n")


# Recall-after
##############
def calculate_recall_at_k(query_id, matching_truth, ground_truth, k=10):
    if query_id in matching_truth:
        matching_docs = list(matching_truth[query_id])[:k]
        relevant_docs = set(ground_truth[query_id])
        retrieved_docs = set(matching_docs)
        not_retrieved_docs = relevant_docs - retrieved_docs
        num_relevant = len(relevant_docs)
        num_retrieved = len(retrieved_docs)
        num_not_retrieved = len(not_retrieved_docs)
        recall = len(relevant_docs.intersection(retrieved_docs)) / num_relevant if num_relevant > 0 else 0.0
        return retrieved_docs, not_retrieved_docs, recall
    else:
        return None, None, None

recall_dict = {}
for query_id in matching_truth:
    retrieved_docs, not_retrieved_docs, recall = calculate_recall_at_k(query_id, matching_truth, ground_truth, k=10)
    if retrieved_docs is not None:
        recall_dict[query_id] = recall

# Write Recall@10 values to a file
file_path = 'recallafter.txt'
with open(file_path, 'w') as f:
    for query_id, recall in recall_dict.items():
        if recall > 0:
            f.write(f"Query {query_id}: {recall}\n")


# MAP-after
###############
def calculate_map(matching_truth, ground_truth, k=10):
    recall_dict = {}
    for query_id in matching_truth:
        retrieved_docs, not_retrieved_docs, recall = calculate_recall_at_k(query_id, matching_truth, ground_truth, k=k)
        if retrieved_docs is not None:
            recall_dict[query_id] = recall

    recall_values = np.array(list(recall_dict.values()))
    sorted_indices = np.argsort(recall_values)[::-1]
    average_precision_values = []
    for i in range(len(sorted_indices)):
        query_id = list(recall_dict.keys())[sorted_indices[i]]
        recall = recall_values[sorted_indices[i]]
        retrieved_docs, not_retrieved_docs, precision = calculate_precision_at_k(query_id, matching_truth, ground_truth, k=k)
        average_precision_values.append(precision)
        if i > 0:
            average_precision_values[i] = max(average_precision_values[i], average_precision_values[i-1])
    MAP = np.mean(average_precision_values)

    return MAP

MAP = calculate_map(matching_truth, ground_truth, k=10)

# Write MAP value to a file
file_path = 'mapafter.txt'
with open(file_path, 'w') as f:
    f.write(f"MAP: {MAP}\n")


# MRR-after
###############

def calculate_mrr(matching_truth, ground_truth, k=10):
    rr_sum = 0
    num_queries = 0

    for query_id in matching_truth:
        retrieved_docs, not_retrieved_docs, recall = calculate_recall_at_k(query_id, matching_truth, ground_truth, k=k)
        if retrieved_docs is not None:
            num_queries += 1
            if query_id in ground_truth:
                relevant_docs = set(ground_truth[query_id])
                for i, doc_id in enumerate(retrieved_docs):
                    if doc_id in relevant_docs:
                        rr_sum += 1 / (i + 1)
                        break

    MRR = rr_sum / num_queries
    return MRR

MRR = calculate_mrr(matching_truth, ground_truth, k=10)

# Write MRR value to a file
file_path = 'mrrafter.txt'
with open(file_path, 'w') as f:
    f.write(f"MRR: {MRR}\n")









