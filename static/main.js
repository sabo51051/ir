const textarea = document.getElementById('textarea');
const autoComplete = document.getElementById('auto-complete');

const getCorrections = async (e) => {
	let str = textarea.value.replace(/\s+/g, ' ')
	let arr = str.trim().split(' ');
	
    if(arr[0] !== "" && str.endsWith(" ")){
        const res = await fetch('/getCorrection', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({words: arr})///////////////////////////////////////////
        });
        
        const data = await res.json();
        
        outputHtml(data);
    }else{
        autoComplete.innerHTML = '';
    }
    
};

textarea.addEventListener('input', getCorrections);

const outputHtml = data =>{

    if(data.length > 0){
        const html = data.map(word => `
            <button type="button" class="card card-body mb-1 w-100 correction" onclick="addToTextarea('${word}')">
                <h4>
                    ${word}
                </h4>
            </button>
        `).join('');
        autoComplete.innerHTML = html;
    }else{
        autoComplete.innerHTML = '';
    }
    
};

const addToTextarea = word => {
	let str = textarea.value.replace(/\s+/g, ' ')
    let arr = str.trim().split(" ");
    
    let lastWord = arr.pop();
    let index = str.lastIndexOf(lastWord)
    	
    textarea.value = textarea.value.substring(0, index) + word + textarea.value.substring(index + lastWord.length, str.length)
    
    autoComplete.innerHTML = '';
    textarea.focus();
};